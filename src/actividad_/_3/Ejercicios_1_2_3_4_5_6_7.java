/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actividad_._3;

import java.util.Scanner;

/**
 *
 * @author Jose
 */
public class Ejercicios_1_2_3_4_5_6_7 {

    /**
     * @param args the command line arguments
     */
   public static void main(String[] args) {
        // TODO code application logic here
        
        Scanner sc = new Scanner(System.in);
        String Alfabeto[][] = new String[26][2];
        Alfabeto[0][0] = "A";
        Alfabeto[1][0] = "B";
        Alfabeto[2][0] = "C";
        Alfabeto[3][0] = "D";
        Alfabeto[4][0] = "E";
        Alfabeto[5][0] = "F";
        Alfabeto[6][0] = "G";
        Alfabeto[7][0] = "H";
        Alfabeto[8][0] = "I";
        Alfabeto[9][0] = "J";
        Alfabeto[10][0] = "K";
        Alfabeto[11][0] = "L";
        Alfabeto[12][0] = "M";
        Alfabeto[13][0] = "N";
        Alfabeto[14][0] = "O";
        Alfabeto[15][0] = "P";
        Alfabeto[16][0] = "Q";
        Alfabeto[17][0] = "R";
        Alfabeto[18][0] = "S";
        Alfabeto[19][0] = "T";
        Alfabeto[20][0] = "U";
        Alfabeto[21][0] = "V";
        Alfabeto[22][0] = "W";
        Alfabeto[23][0] = "X";
        Alfabeto[24][0] = "Y";
        Alfabeto[25][0] = "Z";
        Alfabeto[0][1] = ".-";
        Alfabeto[1][1] = "-...";
        Alfabeto[2][1] = "-.-.";
        Alfabeto[3][1] = "-..";
        Alfabeto[4][1] = ".";
        Alfabeto[5][1] = "..-.";
        Alfabeto[6][1] = "--.";
        Alfabeto[7][1] = "...";
        Alfabeto[8][1] = "..";
        Alfabeto[9][1] = ".---";
        Alfabeto[10][1] = "-.-";
        Alfabeto[11][1] = ".-..";
        Alfabeto[12][1] = "--";
        Alfabeto[13][1] = "-.";
        Alfabeto[14][1] = "---";
        Alfabeto[15][1] = ".--.";
        Alfabeto[16][1] = "--.-";
        Alfabeto[17][1] = ".-.";
        Alfabeto[18][1] = "...";
        Alfabeto[19][1] = "-";
        Alfabeto[20][1] = "..-";
        Alfabeto[21][1] = "...-";
        Alfabeto[22][1] = ".--";
        Alfabeto[23][1] = "-..-";
        Alfabeto[24][1] = "-.--";
        Alfabeto[25][1] = "--..";
        String pal = null;
        System.out.println("Primer ejercicio");
        System.out.println("");
        ac1(Alfabeto);
        System.out.println("");
        System.out.println("Segundo ejercicio");
        System.out.println("\nIngrese una palabra con las letras del alfabeto");
        pal = sc.nextLine();
        char[] ch = pal.toCharArray();
        int asciiValue;
        int i = 0;
        char cChar = ch[i];
        asciiValue = (int) cChar;
        System.out.println("");
        
        Ejercicios_1_2_3_4_5_6_7.ac2(ch, asciiValue);
        String Codcab[][] = new String[12][2];
        Codcab[0][0] = "001-Calkini";
        Codcab[1][0] = "002-Campeche";
        Codcab[2][0] = "003-Carmen";
        Codcab[3][0] = "004-Champoton";
        Codcab[4][0] = "005-Hecelchakan";
        Codcab[5][0] = "006-Hopelchen";
        Codcab[6][0] = "007-Palizada";
        Codcab[7][0] = "008-Tenabo";
        Codcab[8][0] = "009-Escarcega";
        Codcab[9][0] = "010-Calakmul";
        Codcab[10][0] = "011-Candelaria";
        Codcab[11][0] = "012-Seybaplaya";
        int[] habitantes = {52890, 259005, 221094, 83021, 28306, 37777,
            8352, 10665, 54184, 26882, 41194, 15420};

        System.out.println("");
        System.out.println("Tercer ejercicio");
        System.out.println("");
        
        Ejercicios_1_2_3_4_5_6_7.ac3(Codcab, habitantes);
        System.out.println("");

        String[] resp = null;
        String[][] myJuegoGato;
        myJuegoGato = new String[3][3];
        // Asigna variables
        // 1era Línea
        myJuegoGato[0][0] = "0";
        myJuegoGato[0][1] = "0";
        myJuegoGato[0][2] = "0";
        // 2da Línea
        myJuegoGato[1][0] = "0";
        myJuegoGato[1][1] = "0";
        myJuegoGato[1][2] = "0";
        // 3era Línea
        myJuegoGato[2][0] = "0";
        myJuegoGato[2][1] = "0";
        myJuegoGato[2][2] = "0";
        System.out.println("Cuarto ejercicio");
        System.out.println("");
        ac4(resp, myJuegoGato);
        System.out.println("");
        System.out.println("Quinto ejercicio");
        System.out.println("");

        String aOctavos[][] = new String[4][2];

        aOctavos[0][0] = "América";
        aOctavos[1][0] = "Chivas";
        aOctavos[2][0] = "Leon";
        aOctavos[3][0] = "Cruz Azul";
        aOctavos[3][1] = "Toluca";
        aOctavos[2][1] = "Tigres";
        aOctavos[1][1] = "Monterrey";
        aOctavos[0][1] = "Pachuca";

        int gol1[][] = new int[4][2];

        gol1[0][0] = 4;
        gol1[1][0] = 6;
        gol1[2][0] = 3;
        gol1[3][0] = 2;
        gol1[3][1] = 1;
        gol1[2][1] = 2;
        gol1[1][1] = 3;
        gol1[0][1] = 2;

        String aCuartos[][] = new String[2][2];

        aCuartos[0][0] = "América";
        aCuartos[1][0] = "Chivas";
        aCuartos[1][1] = "Cruz Azul";
        aCuartos[0][1] = "Tigres";

        int gol2[][] = new int[2][2];
        gol2[0][0] = 2;
        gol2[1][0] = 3;
        gol2[1][1] = 2;
        gol2[0][1] = 1;

        String aSemifinal[][] = new String[2][2];

        aSemifinal[0][0] = "América";
        aSemifinal[1][0] = "Chivas";
        aSemifinal[1][1] = "Cruz Azul";
        aSemifinal[0][1] = "Tigres";

        int gol3[][] = new int[2][2];
        gol3[0][0] = 4;
        gol3[1][0] = 1;
        gol3[1][1] = 3;
        gol3[0][1] = 2;

        Ejercicios_1_2_3_4_5_6_7.ac5(gol1, gol2, gol3, aOctavos, aCuartos, aSemifinal);
        System.out.println("");
        System.out.println("Sexto ejercicio");
        System.out.println("");

        String S;
        int AH;
        int AM;
        int AHe;
        int AN;
        String[][] Hombres = new String[10][3];

        Hombres[0][0] = "Luke Skywalker";
        Hombres[0][1] = "172";
        Hombres[0][2] = "male";
        AH = Integer.parseInt("172");

        Hombres[1][0] = "Darth Vader";
        Hombres[1][1] = "202";
        Hombres[1][2] = "male";
        AH = AH + Integer.parseInt("202");

        Hombres[2][0] = "Owen Lars";
        Hombres[2][1] = "178";
        Hombres[2][2] = "male";
        AH = AH + Integer.parseInt("178");

        Hombres[3][0] = "Biggs Darklighter";
        Hombres[3][1] = "183";
        Hombres[3][2] = "male";
        AH = AH + Integer.parseInt("183");

        Hombres[4][0] = "Obi-Wan Kenobi";
        Hombres[4][1] = "182";
        Hombres[4][2] = "male";
        AH = AH + Integer.parseInt("182");
        
         Hombres[5][0] = "Yoda";
        Hombres[5][1] = "66";
        Hombres[5][2] = "male";
        AH = AH + Integer.parseInt("66");

        Hombres[6][0] = "Jek Tono Porkins";
        Hombres[6][1] = "180";
        Hombres[6][2] = "male";
        AH = AH + Integer.parseInt("180");

        Hombres[7][0] = "Han Solo";
        Hombres[7][1] = "180";
        Hombres[7][2] = "male";
        AH = AH + Integer.parseInt("180");

        Hombres[8][0] = "Chewbacca";
        Hombres[8][1] = "228";
        Hombres[8][2] = "male";
        AH = AH + Integer.parseInt("228");

        Hombres[9][0] = "Anakin Skywalker";
        Hombres[9][1] = "188";
        Hombres[9][2] = "male";
        AH = AH + Integer.parseInt("188");

        String[][] Mujeres = new String[2][3];

        Mujeres[0][0] = "Leia Organa";
        Mujeres[0][1] = "150";
        Mujeres[0][2] = "female";
        AM = Integer.parseInt("150");

        Mujeres[1][0] = "Beru Whitesun lars";
        Mujeres[1][1] = "165";
        Mujeres[1][2] = "female";
        AM = AM + Integer.parseInt("165");

        String[][] Hermafroditas = new String[1][3];

        Hermafroditas[0][0] = "Jabba Desilijic Tiure";
        Hermafroditas[0][1] = "175";
        Hermafroditas[0][2] = "hermaphrodite";
        AHe = Integer.parseInt("175");

        String[][] Nosexo = new String[3][3];

        Nosexo[0][0] = "R2-D2";
        Nosexo[0][1] = "96";
        Nosexo[0][2] = "n/a";
        AN = Integer.parseInt("96");

        Nosexo[1][0] = "C-3PO";
        Nosexo[1][1] = "167";
        Nosexo[1][2] = "n/a";
        AN = AN + Integer.parseInt("167");

        Nosexo[2][0] = "R5-D4";
        Nosexo[2][1] = "97";
        Nosexo[2][2] = "n/a";
        AN = AN + Integer.parseInt("97");
        System.out.println("Ingrese la letra del sexo de la lista que desea ver, ingrese H para hombres, M para mujeres, He para hermafroditas");
        System.out.println("y N para los que no tienen sexo");
        S = sc.nextLine();
        ac6(Hombres, Mujeres, Hermafroditas, Nosexo, S, AH, AM, AHe, AN);

        System.out.println("");
        System.out.println("Septimo ejercicio");
        System.out.println("");

        int numero;
        
        String[] Alumnos = new String[27];

        Alumnos[0] = "\u001B[30mNombre: Gregorio Duran Julio Cesar Gemelo_Vectoreal: 1001";             //negro
        Alumnos[1] = "\u001B[31mNombre: Gaytan Balan Rodrigo Manuel Gemelo_Vectoreal: 11";              //rojo
        Alumnos[2] = "\u001B[32mNombre: Vargas Cetz William Manuel Gemelo_Vectoreal: 101";              //verde
        Alumnos[3] = "\u001B[33mNombre: Caballero Ramirez Raul Alejandro Gemelo_Vectoreal: 10";         //amarillo
        Alumnos[4] = "\u001B[34mNombre: Leon Argaez Eduardo Alejandro Gemelo_Vectoreal: 11";            //azul
        Alumnos[5] = "\u001B[35mNombre: Salazar Caballero David Romano Gemelo_Vectoreal: 10";           //purpura
        Alumnos[6] = "\u001B[36mNombre: Segovia Alvarado Christopher Raul Gemelo_Vectoreal: 10";        //cian
        Alumnos[7] = "\u001B[37mNombre: Duarte Hernández Eduardo Ivan Gemelo_Vectoreal: 11";            //blanco
        Alumnos[8] = "\u001B[30mNombre: Hernandez Crisanto Brayan Rafael Gemelo_Vectoreal: 1001";       //negro
        Alumnos[9] = "\u001B[31Nombre: Zaragoza Burgos Yazuri Elizabeth Gemelo_Vectoreal: 111";         //rojo
        Alumnos[10] = "\u001B[32Nombre: Uc May Jose Maria Andres Gemelo_Vectoreal: 1000";                //verde
        Alumnos[11] = "\u001B[33Nombre: Cach Laines Orlando Adriel Gemelo_Vectoreal: 101";               //amarillo
        Alumnos[12] = "\u001B[34Nombre: Colli Aguilar Daniel Alejandro Gemelo_Vectoreal: 110";           //azul
        Alumnos[13] = "\u001B[35Nombre: Castillo Canul Gustavo Moises Gemelo_Vectoreal: 100";            //purpura
        Alumnos[14] = "\u001B[36Nombre: De La Cruz Santos Alexander Gemelo_Vectoreal: 11";               //cian
        Alumnos[15] = "\u001B[37Nombre: Berzunza Poot Cristian Gemelo_Vectoreal: 11";                    //blanco
        Alumnos[16] = "\u001B[30Nombre: Cahuich Ake Darlin Edey Gemelo_Vectoreal: 1";                    //negro
        Alumnos[17] = "\u001B[31Nombre: Solis May Josue Israel Gemelo_Vectoreal: N/A";                   //rojo
        Alumnos[18] = "\u001B[32Nombre: Sánchez Rosario Ricardo Daniel Gemelo_Vectoreal: 11";            //verde
        Alumnos[19] = "\u001B[33Nombre: Trejo Moo Cinthia Guadalupe Gemelo_Vectoreal: 1000";             //amarillo
        Alumnos[20] = "\u001B[34Nombre: Pech Brito Victor Manuel Gemelo_Vectoreal: 1100";                //azul
        Alumnos[21] = "\u001B[35Nombre: Lopez Lopez Erika Paola Gemelo_Vectoreal: 100";                  //purpura
        Alumnos[22] = "\u001B[36Nombre: Hernandez Estrella Edwin Adrian Gemelo_Vectoreal: 1000";         //cian
        Alumnos[23] = "\u001B[37Nombre: Tamay De Los Santos Monica Estefania Gemelo_Vectoreal: 111";     //blanco
        Alumnos[24] = "\u001B[30Nombre: Pacheco Canul Cecilia Gabriela Estefania Gemelo_Vectoreal: 100"; //negro 
        Alumnos[25] = "\u001B[31Nombre: Sierra Tzec Brian Enrique Estefania Gemelo_Vectoreal: 100";      //rojo
        Alumnos[26] = "\u001B[32Nombre: Bacab Ic Frida Marian Gemelo_Vectoreal: 1000";                   //verde

        System.out.println("Introduzca un número entre el 1 y 27 ");
        numero = sc.nextInt();
        ac7(numero, Alumnos);

    }
   
public static void ac1(String[][] Alfabeto) {

        System.out.println("Letras del alfabeto y su forma morse");

        for (int i = 0; i < 26; i++) {
            System.out.println(Alfabeto[i][0] + " = " + Alfabeto[i][1]);
        }
    }

    public static void ac2(char[] ch, int asciiValue) {

        Scanner sc = new Scanner(System.in);

        for (Character c : ch) {

            System.out.println("La letra " + (char) (c + 0) + " = " + (c + 0));

        }
    }

    public static void ac3(String[][] Codcab, int[] habitantes) {
        for (int i = 0; i < 12; i++) {
            System.out.println(Codcab[i][0] + "   " + Codcab[i][1] + "   " + habitantes[i]);

        }

    }

    public static void ac4(String[] resp, String[][] myJuegoGato) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese un número del 0 al 2, una coma y H para horizontal, V para vertical ");

        resp = sc.nextLine().split(",");
        
 switch (resp[1]) {
            case "H":
                for (int i = 0; i < 3; i++) {
                    myJuegoGato[Integer.parseInt(resp[0])][i] = "X";
                }
                break;
            case "V":
                for (int i = 0; i < 3; i++) {
                    myJuegoGato[i][Integer.parseInt(resp[0])] = "X";
                }
                break;
            default:
                System.out.println(resp[1] + "No es un valor valido");
                break;
        }

        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                System.out.print(myJuegoGato[x][y]);
            }
            System.out.println();
        }
    }

    public static void ac5(int[][] gol1, int[][] gol2, int[][] gol3, String[][] aOctavos, String[][] aCuartos, String[][] aSemifinal) {
        Scanner sc = new Scanner(System.in);
        if (gol1[0][0] > gol1[0][1]) {
            System.out.println("Resultados de octavos de final");
            System.out.println(aOctavos[0][0] + " es el ganador contra " + aOctavos[0][1] + " con " + gol1[0][0] + " goles");
        } else {
            System.out.println(aOctavos[0][1] + " es el ganador contra " + aOctavos[0][0] + " con " + gol1[0][1] + " goles");
        }

        if (gol1[1][0] > gol1[1][1]) {
            System.out.println(aOctavos[1][0] + " es el ganador contra " + aOctavos[1][1] + " con " + gol1[1][0] + " goles");
        } else {
            System.out.println(aOctavos[1][1] + " es el ganador contra " + aOctavos[1][0] + " con " + gol1[1][1] + " goles");
        }
        
 if (gol1[2][0] > gol1[2][1]) {
            System.out.println(aOctavos[2][0] + " es el ganador contra " + aOctavos[2][1] + " con " + gol1[2][0] + " goles");
        } else {
            System.out.println(aOctavos[2][1] + " es el ganador contra " + aOctavos[2][0] + " con " + gol1[2][1] + " goles");
        }

        if (gol1[3][0] > gol1[3][1]) {
            System.out.println(aOctavos[3][0] + " es el ganador contra " + aOctavos[3][1] + " con " + gol1[3][0] + " goles");
        } else {
            System.out.println(aOctavos[3][1] + " es el ganador contra " + aOctavos[3][0] + " con " + gol1[3][1] + " goles");
        }

        System.out.println("");
        if (gol2[0][0] > gol2[0][1]) {
            System.out.println("Resultados de cuarto de final");
            System.out.println(aCuartos[0][0] + " es el ganador contra " + aCuartos[0][1] + " con " + gol2[0][0] + " goles");
        } else {
            System.out.println(aCuartos[0][1] + " es el ganador contra " + aCuartos[0][0] + " con " + gol2[0][1] + " goles");
        }
        if (gol2[1][0] > gol2[1][1]) {
            System.out.println(aCuartos[1][0] + " es el ganador contra " + aCuartos[1][1] + " con " + gol2[1][0] + " goles");
        } else {
            System.out.println(aCuartos[1][1] + " es el ganador contra " + aCuartos[1][0] + " con " + gol2[1][1] + " goles");
        }

        System.out.println("");
        if (gol3[0][0] > gol3[0][1]) {
            System.out.println("Resultados de semfinal");
            System.out.println(aSemifinal[0][0] + " es el ganador contra " + aSemifinal[0][1] + " con " + gol3[0][0] + " goles");
        } else {
            System.out.println(aSemifinal[0][1] + " es el ganador contra " + aSemifinal[0][0] + " con " + gol3[0][1] + " goles");
        }
        if (gol3[1][0] > gol3[1][1]) {
            System.out.println(aSemifinal[1][0] + " es el ganador contra " + aSemifinal[1][1] + " con " + gol3[1][0] + " goles");
        } else {
            System.out.println(aSemifinal[1][1] + " es el ganador contra " + aSemifinal[1][0] + " con " + gol3[1][1] + " goles");
        }

    }
    
public static void ac6(String[][] Hombres, String[][] Mujeres, String[][] Hermafroditas, String[][] Nosexo, String S, int AH, int AM, int AHe, int AN) {

        String H = "H";
        String M = "M";
        String He = "He";
        String N = "N";

        if (S.equals(H)) {
            int promedio;
            for (int i = 0; i < Hombres.length; i++) {
                for (int j = 0; j < Hombres[i].length; j++) {
                    System.out.println(Hombres[i][j]);
                }
            }
            System.out.println("\nEl personaje mas bajo es Yoda con una altura de 66 metros");
            System.out.println("El mas alto es Chewbacca con una altura de 228 metros");
            System.out.println("La suma de la altura de todos los hombres es: " + AH + " metros");

            promedio = AH / 10;
            System.out.println("El promedio de la altura de los hombres es: " + promedio + " metros");
        } else if (S.equals(M)) {
            int promedio;
            for (int i = 0; i < Mujeres.length; i++) {
                for (int j = 0; j < Mujeres[i].length; j++) {
                    System.out.println(Mujeres[i][j]);

                }
            }
            System.out.println("\nEl personaje mas bajo es Leia Organa con una altura de 150 metros");
            System.out.println("El mas alto es Beru Whitesun lars con una altura de 165 metros");
            System.out.println("La suma de la altura de todas las mujeres es: " + AM + " metros");

            promedio = AM / 2;
            System.out.println("El promedio de la altura de las mujeres es: " + promedio + " metros");

        } else if (S.equals(He)) {
                int promedio;
            for (int i = 0; i < Hermafroditas.length; i++) {
                for (int j = 0; j < Hermafroditas[i].length; j++) {
                    System.out.println(Hermafroditas[i][j]);
                }
            }
            System.out.println("\nEl personaje mas bajo es Jabba Desilijic Tiure con una altura de 175 metros");
            System.out.println("El mas alto es Jabba Desilijic Tiure con una altura de 75 metros");
            System.out.println("La suma de la altura de todos los Hermafroditas es: " + AHe + " metros");

            promedio = AHe / 1;
            System.out.println("El promedio de la altura de los Hermafroditas es: " + promedio + " metros");

        } else if (S.equals(N)) {
            int promedio;
            for (int i = 0; i < Nosexo.length; i++) {
                for (int j = 0; j < Nosexo[i].length; j++) {
                    System.out.println(Nosexo[i][j]);
                }
            }
            System.out.println("\nEl personaje mas bajo es R2-D2 con una altura de 96 metros");
            System.out.println("El mas alto es C-3PO con una altura de 167 metros");
            System.out.println("La suma de la altura de todos los N/A es: " + AN + " metros");

            promedio = AN / 3;
            System.out.println("El promedio de la altura de los N/A es: " + promedio + " metros");

        } else {
            System.out.println("La letra ingresada no corresponde a ninguna lista o no se ingreso una letra");
        }

    }
 public static void ac7(int numero, String[] alumnos) {

        if (numero > 0 && numero < 28) {
            System.out.print("El numero es: " + numero + " ");
            numero = numero - 1;
            System.out.println("Los datos del numero son: \n" + alumnos[numero]);
        } else {
            System.out.println("El numero que ingreso no entra en el rango de 1 y 27 o no es un numero");
        }
    }

}
